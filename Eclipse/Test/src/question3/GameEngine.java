package question3;

import java.util.Random;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class GameEngine implements EventHandler<ActionEvent> {
	
	private TextField bet;
	private Text message;
	private String action;
	private Text money;
	private int moneyHold = 100;
	private Random rand = new Random();
	
	public GameEngine(TextField bet, Text message, Text money, String action) {
		this.bet = bet;
		this.message = message;
		this.money = money;
		this.action = action;
	}
	
	@Override
	public void handle(ActionEvent arg) {
		
		String result;
		try {
			
			if(!bet.getText().equals("")) {
				
				if(Integer.parseInt(bet.getText()) > 0) {
					
					if(Integer.parseInt(bet.getText()) < moneyHold) {
						
						int randomNumber = rand.nextInt(2);
						if(randomNumber == 0) {
							
							result = "Heads";
						} else {
							
							result = "Tails";
						}
						if(result.equals(action)) {
							
							message.setText("You won and doubled your money");
							moneyHold += Integer.parseInt(bet.getText());
							money.setText(Integer.toString(moneyHold));
						} else {
							
							message.setText("You lost the bet and your money. Feels bad.");
							moneyHold -= Integer.parseInt(bet.getText());
							money.setText(Integer.toString(moneyHold));
						}
					} else {
						
						message.setText("You don't have the money pal.");
					}
				} else {
					
					message.setText("The bet must be greater than 0.");
				}
			} else {
				
				message.setText("You need to enter a bet.");
			}
		} catch(Exception e) {
			
			e.printStackTrace();
		}
	}

}
