package question3;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CoinFlipClient extends Application {

	private GameEngine action;
	public static void main(String[] args) {

		launch(args);
	}

	@Override
	public void start(Stage stage) {
		
		//Declares the Buttons and TextFields
		TextField bet = new TextField();
		bet.setPrefWidth(100);
		Text message = new Text("");
		Text money = new Text("100");
		Button heads = new Button("Heads");
		action = new GameEngine(bet, message, money, "Heads");
		heads.setOnAction(action);
		Button tails = new Button("Tails");
		action = new GameEngine(bet, message, money, "Tails");
		tails.setOnAction(action);
		
		//Adds the components to Hboxes
		HBox betField = new HBox();
		betField.getChildren().add(bet);
		HBox betButtons = new HBox();
		betButtons.getChildren().addAll(heads, tails);
		HBox textBar = new HBox();
		textBar.getChildren().addAll(money, message);
		textBar.setSpacing(10);
		
		//Adds Hboxes to a VBox then to a Grou[ and then to the Scene
		VBox overall = new VBox();
		overall.getChildren().addAll(betField, betButtons, textBar);
		Group root = new Group();
		root.getChildren().add(overall);
		Scene scene = new Scene(root, 400, 400); 
		scene.setFill(Color.WHITE);

		//Sets up the Stage
		stage.setTitle("Coin Flip"); 
		stage.setScene(scene); 
		stage.show(); 
	}
}
