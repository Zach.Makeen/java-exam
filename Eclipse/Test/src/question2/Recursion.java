package question2;

public class Recursion {

	public static void main(String[] args) {

		String[] words = {"squirrel", "hello", "headquarters", "computer", "prerequisite", "paper", "squirrel"};
		System.out.println(recursiveCount(words, 3));
	}
	
	public static int recursiveCount(String[] words, int n) {
		
		int count = 0;
		count = recursiveCount(words, n, count);
		return count;
	}
	
	private static int recursiveCount(String[] words, int n, int count) {
		
		if(n % 2 == 0 && n < words.length) {
			
			count = recursiveCount(words, (n + 2), count);
			if(words[n].contains("q")) {
				
				count++;
			}
		}
		if(n % 2 == 1 && n < words.length) {
			
			count = recursiveCount(words, (n + 1), count);
			if(words[n].contains("q")) {
				
				count++;
			}
		}
		return count;
	}

}
